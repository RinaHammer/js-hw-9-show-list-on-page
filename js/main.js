/*Теоретичні питання та відповіді.
    1. Опишіть, як можна створити новий HTML тег на сторінці.
    Для створення нового HTML тегу можна використовувати метод document.createElement(). 
    Коли ми хочемо створити новий тег, наприклад, <h1>, ми використовуємо код document.createElement("h1").
   
    2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
    Перший параметр функції insertAdjacentHTML в JavaScript визначає, куди ми хочемо вставити новий HTML-код. Це може бути одне з чотирьох значень:
    'beforebegin': Вставити HTML перед початком вмісту елемента (зовнішній брат елемента).
    'afterbegin': Вставити HTML в початок вмісту елемента (внутрішній елемент).
    'beforeend': Вставити HTML в кінець вмісту елемента (внутрішній елемент).
    'afterend': Вставити HTML після елемента (зовнішній брат елемента).
    

    3.Як можна видалити елемент зі сторінки?
    Щоб видалити елемент зі сторінки, можна використати метод remove(). Наприклад, якщо у нас є елемент з ідентифікатором "myElement", можемо видалити його так: 
    const elementToRemove = document.getElementById('myElement'); elementToRemove.remove(); 
    Це зробить елемент невидимим і видалить його з DOM-дерева.*/

/*Завдання
Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
Приклади масивів, які можна виводити на екран:

["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

["1", "2", "3", "sea", "user", 23];

Можна взяти будь-який інший масив.
Необов'язкове завдання підвищеної складності:
Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:

["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.

Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.*/

function createList(array, parentElement = document.body) {
  let list = document.createElement("ul");
  parentElement.appendChild(list);
  array.forEach((element) => {
    if (!Array.isArray(element)) {
      const listItem = document.createElement("li");
      listItem.innerText = element;
      list.appendChild(listItem);
    } else {
      createList(element, list);
    }
  });

  return parentElement.appendChild(list);
}
let timeleft = 2;
let downloadTimer = setInterval(function () {
  if (timeleft <= 0) {
    clearInterval(downloadTimer);
    document.getElementById("countdown").innerHTML = "Finished";
  } else {
    document.getElementById("countdown").innerHTML =
      timeleft + " seconds to clear the document";
  }
  timeleft -= 1;
}, 1000);
setTimeout(() => (document.body.innerHTML = ""), 3000);

createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
createList(["1", "2", "3", "sea", "user", 23]);
createList([
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
]);
createList([
  "Madrid",
  ["Adolfo Suárez Madrid-Barajas", "Cuatro Vientos"],
  "Barcelona",
  "Tenerife",
  ["Tenerife Sur", "Tenerife Norte-Ciudad de La Laguna"],
  "Malaga",
]);
